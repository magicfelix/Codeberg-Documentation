---
eleventyNavigation:
  key: CodebergPages
  title: Codeberg Pages
  icon: server
  order: 60
---

Codeberg Pages allows you to easily publish static websites with a human-friendly address (`{username}.codeberg.page`) via Git on Codeberg.
Follow these simple steps below to get started, or check out the advanced usage below.

1. Create a public repository named 'pages' in your user account or organization.
2. Create static content, HTML, stylesheets, fonts or images. Name the homepage file `index.html`.
3. Push your content to the default branch of the new repository.
4. You should now be able to access your content by visiting `{username}.codeberg.page`.

See also [https://codeberg.page/](https://codeberg.page) or the [Troubleshooting](troubleshooting) page.

## Advanced Usage: Canonical URLs

The Codeberg Pages server responds to four different URLs:

- `https://raw.codeberg.page/username/reponame/`: raw content, uses correct MIME types (HTML is forbidden though) and is accessible with CORS.
- `https://username.codeberg.page`: user page, points the default branch of a user's or organization's `pages` repository
- `https://username.codeberg.page/reponame/`: repo page, points to the `pages` branch of the repository
- `https://example.org`: custom domain, points to a repo of choice as outlined below

In all cases, you can append a branch using an `@` (e.g. `https://username.codeberg.page/@develop/README.md`). If the branch name contains a slash (`/`), they need to be replaced with a tilde (`~`) (e.g. the branch `docs/develop` can be accessed via `https://username.codeberg.page/@docs~develop/README.md`).

## Custom Domains

> **Currently known pitfalls for failed certificates:**  
> - you must either not have a [CAA record](https://en.wikipedia.org/wiki/DNS_Certification_Authority_Authorization#Record), or explicitly allow [LetsEncrypt](https://letsencrypt.org) there

For custom domains, two things are required:
- a `.domains` file in the repository (in the branch in question), containing a list of *all* domains that shall be usable to access that repository, according to the following rules:
  - One domain per line, you can leave lines empty and comment out lines with `#`.
  - The first domain is the main one where all other domains in the file will be redirected to.
  - The rest of the list includes also all relevant `*.codeberg.page` domains for the specific repository.
- a CNAME record pointing to one of the following targets:
  - `username.codeberg.page` → https://username.codeberg.page
  - `reponame.username.codeberg.page` → https://username.codeberg.page/reponame/
  - `branchname.reponame.username.codeberg.page` → https://username.codeberg.page/reponame/@branchname/
  
If you can't use a CNAME record to configure the target (e.g. for a zone root), you can use an A/AAAA/ALIAS record to `codeberg.page` with an additional TXT record for the target (just as shown above for CNAME records).

The following sub-sections include a few examples of the different alternatives, assuming the following:

- we can add/modify DNS records in domain `example.com`.
- Our Codeberg username is `frida`, and we want to publish pages for:
    - `frida/pages`, with a *Personal* static site inside branch `pages`;
    - `frida/colormix`, with a *Project* site (again, inside branch `pages`).

All considerations regarding a *Personal* site also apply to an *Organization* site, so these two cases will be addressed together.

### Personal (or Organization) site, third level domain

In this case, we want our *Personal* pages available at the URL `https://myself.example.com`.

The `.domains` file contains the following:

```
myself.example.com
frida.codeberg.page
pages.frida.codeberg.page
pages.pages.frida.codeberg.page
```

For the DNS configuration:

- if CNAME can be used, one single DNS record will suffice:
    - name `myself.example.com`, type `CNAME`, data `frida.codeberg.page`

- otherwise, if ALIAS can be used, two DNS records will be needed:
    - name `myself.example.com`, type `ALIAS`, data `codeberg.page`
    - name `myself.example.com`, type `TXT`, data `frida.codeberg.page`

- otherwise, A/AAAA records must be used, together with one TXT record:
    - name `myself.example.com`, type `A`, data `217.197.91.145`
    - name `myself.example.com`, type `AAAA`, data `2001:67c:1401:20f0::1`
    - name `myself.example.com`, type `TXT`, data `frida.codeberg.page`

### Personal/Organization site, apex domain

In this case, we want our *Personal*/*Organization* pages available at the URL `https://example.com`.

The `.domains` file contains the following:

```
example.com
frida.codeberg.page
pages.frida.codeberg.page
pages.pages.frida.codeberg.page
```

For the DNS configuration, the CNAME SHOULD NOT be used, so:

- if ALIAS can be used, two DNS records will be needed:
    - name `example.com`, type `ALIAS`, data `codeberg.page`
    - name `example.com`, type `TXT`, data `frida.codeberg.page`

- otherwise, A/AAAA records must be used, together with one TXT record:
    - name `example.com`, type `A`, data `217.197.91.145`
    - name `example.com`, type `AAAA`, data `2001:67c:1401:20f0::1`
    - name `example.com`, type `TXT`, data `frida.codeberg.page`

### Project site, third-level domain

In this case, we want our *Project* pages available at the URL `https://colormix-app.example.com`.

The `.domains` file contains the following:

```
colormix-app.example.com
colormix.frida.codeberg.page
pages.colormix.frida.codeberg.page
```

For the DNS configuration:

- if CNAME can be used, one single DNS record will suffice:
    - name `colormix-app.example.com`, type `CNAME`, data `colormix.frida.codeberg.page`

- otherwise, if ALIAS can be used, two DNS records will be needed:
    - name `colormix-app.example.com`, type `ALIAS`, data `codeberg.page`
    - name `colormix-app.example.com`, type `TXT`, data `colormix.frida.codeberg.page`

- otherwise, A/AAAA records must be used, together with one TXT record:
    - name `colormix-app.example.com`, type `A`, data `217.197.91.145`
    - name `colormix-app.example.com`, type `AAAA`, data `2001:67c:1401:20f0::1`
    - name `colormix-app.example.com`, type `TXT`, data `colormix.frida.codeberg.page`

### Project site, apex domain

In this case, we want our *Project* pages available at the URL `https://example.com`.

> Note: this would be incompatible with using the apex `example.com` for other purposes, e.g. for the *Personal*/*Organization* example discussed before.

The `.domains` file contains the following:

```
example.com
colormix.frida.codeberg.page
pages.colormix.frida.codeberg.page
```

For the DNS configuration, CNAME SHOULD NOT be used:

- if ALIAS can be used, two DNS records will be needed:
    - name `example.com`, type `ALIAS`, data `codeberg.page`
    - name `example.com`, type `TXT`, data `colormix.frida.codeberg.page`

- otherwise, A/AAAA records must be used, together with one TXT record:
    - name `example.com`, type `A`, data `217.197.91.145`
    - name `example.com`, type `AAAA`, data `2001:67c:1401:20f0::1`
    - name `example.com`, type `TXT`, data `colormix.frida.codeberg.page`



## Do you have questions, feedback or have you found a bug?

The source code for Codeberg Pages is maintained over at the [Pages Server repository](https://codeberg.org/Codeberg/pages-server); feel free to head there to provide some feedback, suggestions, bug reports or even patches.
If you need general community support or have questions, [Codeberg/Community](https://codeberg.org/Codeberg/Community) is a better place to ask, as more people will be watching there to help you out!
We really appreciate your contribution.

## Installing Pages for your own Gitea instance

Codeberg Pages works with any Gitea host out there. So if you are running your own Gitea, you can absolutely run it yourself and help with the development.
Check out the [Pages Server repository](https://codeberg.org/Codeberg/pages-server) for more information.
