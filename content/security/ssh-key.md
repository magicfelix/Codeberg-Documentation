---
eleventyNavigation:
  key: SSHKey
  title: Adding an SSH key to your account
  parent: Security
  order: 20
---

It is recommended to use one key per client. This means that if you access your Codeberg repository from your home PC, your laptop and your office PC you should generate separate keys for each machine.

## Generating an SSH key pair

1. Open Terminal on Linux/macOS, or Git Bash on Windows.

2. Paste the text below:

    ```shell
    ssh-keygen -t ed25519 -a 100
    ```

    This will generate a new SSH key.
    You can also add a comment to help you identify the client with `-C "comment here"`.

    ```shell
    > Generating public/private ed25519 key pair.
    ```

3. When you're prompted to "Enter a file in which to save the key", press <kbd>Enter</kbd>. This accepts the default file location:

    ```shell
    > Enter file in which to save the key (/home/knut/.ssh/id_ed25519): [Press enter]
    ```

4. You will be asked for a passphrase; enter one if you'd like, or leave the prompt empty.

Your private key can be protected by a passphrase. This adds a layer of authentication that increases security. Be aware that this will only be helpful for certain attack scenarios and does not offer 100% protection. It is recommended to keep your private key safe and - well - private.

## Add the SSH key to Codeberg

1. Copy the SSH key to your clipboard. You must only copy the public key not the private one. You can identify it by the `.pub` suffix. By default, you can find the public key in `$HOME/.ssh/id_ed25519.pub`.

    On Linux you can use `xclip` on the command line. You may need to install it from your package manager.
    ```shell
    $ xclip -selection clipboard < ~/.ssh/id_ed25519.pub
    ```

    On Windows you can use `clip`.
    ```shell
    $ clip < ~/.ssh/id_ed25519.pub
    ```

    On macOS you can use `pbcopy`.
    ```shell
    $ pbcopy < ~/.ssh/id_ed25519.pub
    ```

    These commands will copy the contents of id_ed25519.pub (your SSH public key) to your clipboard.

    > Alternatively you can locate the hidden .ssh folder, open the file in your favorite text editor, and copy it to your clipboard.

2. Navigate to your user settings
<picture>
  <source srcset="/assets/images/security/user-settings.webp" type="image/webp">
  <img src="/assets/images/security/user-settings.png" alt="User Settings">
</picture>

3. Go to the section __SSH / GPG Keys__ and click on __Add key__.

<picture>
  <source srcset="/assets/images/security/ssh-key/add-ssh-key.webp" type="image/webp">
  <img src="/assets/images/security/ssh-key/add-ssh-key.png" alt="SSH Key Settings">
</picture>

4. Give an appropriate name for the key.
5. Paste your key string into __Content__ field.
6. Click the __Add key__ button.

> You can always access your SSH public keys from `https://codeberg.org/username.keys`, substituting in your Codeberg username.

## Test the SSH connection

Do this simple test:

```shell
$ ssh -T git@codeberg.org
```

The output should look like this:

```text
Hi there! You've successfully authenticated, but Gitea does not provide shell access.
If this is unexpected, please log in with password and setup Gitea under another user.
```

*Note: All Codeberg users share a single Unix user named `git` which is used to check out repositories. Depending on the key provided, permission is granted or denied. You can check out all repositories with your key which you have permission for. You can push code to all repositories where you have write access.*

## Verifying your SSH Key

Anyone can add a random SSH key; but fortunately, Codeberg provides a mechanism to verify that the key belongs to you.
Every keypair consists of a public and a private key that are connected to one another. Using this private key, you can sign the provided message. If the signed message is valid, Codeberg can confirm that the added key is yours. 

1. Go to the [SSH/GPG Keys tab](https://codeberg.org/user/settings/keys) in your Codeberg settings.
2. Click on the __Verify__ button next to the SSH key you would like to verify.
3. Codeberg will show a token. Under its text box copy the command, and replace `/path_to_your_pubkey` with the correct path of your public key.
4. Copy the output, beginning with `-----BEGIN SSH SIGNATURE-----` and ending with `-----END SSH SIGNATURE-----`.
5. Paste it into the large textbox and click the __Verify__ button.

## Avoid re-typing the passphrase

Assuming you've created a secure key with a passphrase, SSH will prompt you for your passphrase for every connection. Common desktop environments like macOS or GNOME will offer you to cache your passphrase via an SSH agent.

If you are working at the command line, you can alternatively do this directly:

```shell
$ eval $(ssh-agent)
$ ssh-add # enter your passphrase once, then it is cached.
```

> **Attribution**  
> This guide is derived from [GitHub Docs](https://docs.github.com), used under CC-BY 4.0.
